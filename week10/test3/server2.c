/* 
 * echoserverp.c - A concurrent echo server based on processes
 */
/* $begin echoserverpmain */
#include "csapp.h"
void echo(int connfd,char *haddrp);

void sigchld_handler(int sig) //line:conc:echoserverp:handlerstart
{
    while (waitpid(-1, 0, WNOHANG) > 0)
	;
    return;
} //line:conc:echoserverp:handlerend

int main(int argc, char **argv) 
{
    int listenfd, connfd, port;
    char *haddrp;
    socklen_t clientlen=sizeof(struct sockaddr_in);
    struct sockaddr_in clientaddr;

    if (argc != 2) {
	fprintf(stderr, "usage: %s <port>\n", argv[0]);
	exit(0);
    }
    port = atoi(argv[1]);

    Signal(SIGCHLD, sigchld_handler);
    listenfd = Open_listenfd(port);
    while (1) {
	connfd = Accept(listenfd, (SA *) &clientaddr, &clientlen);
	if (Fork() == 0) { 
	    haddrp = inet_ntoa(clientaddr.sin_addr);
	    Close(listenfd); /* Child closes its listening socket */
	    echo(connfd,haddrp);    /* Child services client */ //line:conc:echoserverp:echofun
	    Close(connfd);   /* Child closes connection with client */ //line:conc:echoserverp:childclose
	    exit(0);         /* Child exits */
	}
	Close(connfd); /* Parent closes connected socket (important!) */ //line:conc:echoserverp:parentclose
    }
}
/* $end echoserverpmain */
void echo(int connfd,char *haddrp) 
{
    static char timestr[40];  
    time_t t;  
    struct tm *nowtime;
    time(&t);  
    nowtime = localtime(&t);  
    strftime(timestr,sizeof(timestr),"%Y-%m-%d %H:%M:%S",nowtime);
    size_t n; 
    char buf[MAXLINE]; 
    rio_t rio;

    Rio_readinitb(&rio, connfd);
    while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) { //line:netp:echo:eof
	printf("server received %d bytes\n", (int)n);
	printf("客户端IP:%s\n",haddrp);
	printf("服务器实现者学号:20155339\n");
	printf("当前时间:%s\n",timestr);
	printf("\n");
	Rio_writen(connfd, buf, n);
    }
}

