/*
 * echoclient.c - An echo client
 */
/* $begin echoclientmain */
#include "./csapp.h"

int main(int argc, char **argv) 
{
    int clientfd, port;
    char *host, buf[MAXLINE];
    rio_t rio;

    if (argc != 3) {
	fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
	exit(0);
    }
    host = argv[1];
    port = atoi(argv[2]);

    clientfd = Open_clientfd(host, port);
    Rio_readinitb(&rio, clientfd);

    while (Fgets(buf, MAXLINE, stdin) != NULL) {
	static char timestr[40];
    time_t t;
    struct tm *nowtime;
    time(&t);
    nowtime = localtime(&t);
    strftime(timestr,sizeof(timestr),"%Y-%m-%d %H:%M:%S",nowtime);
    printf("客户端IP:127.0.0.1\n");
    printf("服务器实现者学号:20155339\n");
        printf("当前时间:%s\n",timestr);
        printf("\n");
    Rio_writen(clientfd, buf, strlen(buf));
        Rio_readlineb(&rio, buf, MAXLINE);
        Fputs(buf, stdout);

    }

    Close(clientfd); //line:netp:echoclient:close
    exit(0);
}
/* $end echoclientmain */

