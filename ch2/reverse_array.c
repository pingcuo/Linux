#include<stdio.h>
#define N 10
main()
{
int a[N],i,n;
printf("please input the number:\n");
scanf("%d",&n);
printf("please input any number:\n");
for(i=0;i<n;i++)
{
scanf("%d",&a[i]);
}
printf("before:");
for(i=0;i<n;i++)
{
printf("%d ",a[i]);
}
reverse_array(a,n);
printf("after:");
for(i=0;i<n;i++)
{
printf("%d ",a[i]);
}
printf("\n");
}
void inplace_swap(int *x,int *y){
*y=*x^*y;
*x=*x^*y;
*y=*x^*y;
}
void reverse_array(int a[],int cnt)
{
int first,last;
for(first=0,last=cnt-1;first<last;first++,last--)
{
inplace_swap(&a[first],&a[last]);
}
}
