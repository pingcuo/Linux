#include <unistd.h>
#include<stdio.h>
#include<stdlib.h>
#define N 128
void eval(char *cmdline);
int parseline(char *buf,char **argv);
int builtin_command(char **argv);
int main()
{
char cmdline[N];
while(1)
{
printf("> ");
fgets(cmdline,N,stdin);
if(feof(stdin))
exit(0);
eval(cmdline);
}
}
