#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef unsigned float_bits;
float_bits float_i2f(int i){
if(!i)
return 0;
unsigned sign=0;
unsigned x=i;
if(i&0x80000000){
sign=1;
x=~x+1;
}
unsigned c=0;
while (!(x&0x00000001))
{
c=c>>1;
++c;
}
unsigned exp= x+127;
x=x<<1;
x=(x>>9)+((x&0x100)&&((x&0x200)||(x&0xff)));
if(x&0x800000) 
++exp;
return (sign<<31) | (exp<<23) | (x&0x7fffff);
}
int main()
{float x=12345;
float b;
b=float_i2f(x);
printf("%x\n",b);
}
